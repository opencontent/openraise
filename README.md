# openraise

Una piattaforma di crowdfunding per pubbliche amministrazioni

## Motivazione

Il Piano Triennale della PA Italiana ha lanciato con grande forza il tema dello
sviluppo cooperativo e aperto di software utile alla PA per lo svolgimento dei 
propri compiti istituzionali. Molto software opensource è già stato rilasciato
come previsto dalle Linee Guida per il Riuso sotto licenza opensource e
pubblicamente disponibile sulle più diffuse piattaforme di code-hosting. 
Il catalogo del software rilasciato si trova all'indirizzo:
https://developers.italia.it/

Sebbene sia prevista dalle Linee Guida, e' ancora poco diffusa la pratica di 
cooperazione di amministrazioni
diverse per lo sviluppo di software pubblico, ogni Ente tende a gestire 
internamente l'evoluzione dei propri software oppure a delegarne completamente
all'esterno lo sviluppo e mantenimento.

La piattaforma openraise ha lo scopo di facilitare lo sviluppo e il mantenimento
del software opensource e di interesse pubblico, mediante il finanziamento di 
feature e evoluzioni.

Vista la peculiarità della PA, nessuna delle piattaforme esistenti può 
soddisfare questa necessità, perché le PA non possono usare i classici sistemi
di pagamento come stripe o paypal per pre-finanziare lo sviluppo di un software.
Le amministrazioni potranno quindi _impegnarsi_ a contribuire allo sviluppo di 
un determinato software, ma l'impegno si concretizzerà solo successivamente,
con un contratto da stabilire con il maintainer nel momento in cui tutti i fondi
necessari sono stati assicurati al progetto.

## Stato del progetto

Just a readme :)

## Come funziona

Su openraise è possibile finanziare lo sviluppo di feature specifiche per 
software pubblicati su developers italia.

Il maintainer di un software opensource indica la propria roadmap di sviluppo
 e il costo necessario per le feature sviluppabili.

Un Ente interessato a finanziare interamente o in parte una nuova funzionalità può
selezionare la feature a cui è interessata e indicare il massimo finanziamento
che è disposta a erogare. Nell'immediato non vi è alcuna transazione in denaro,
si tratta solo della _promessa_ che l'Ente fa relativa a quella feature 
specifica.

Quando viene raggiunta la quota prestabilita il maintaner potrà effettuare un
contratto con gli Enti coinvolti, secondo le modalità più opportune.

Ogni feature finanziabile ha un tempo prestabilito di 1 anno per ricevere i 
finanziamenti necessari, dopo questo tempo la feature decade e tutte le promesse
di finanziamento vengono cancellate.

## Homepage

Sulla homepage vengono elencate le feature che possono essere finanziate e per
ognuna di esse un insieme di informazioni correlate:

 * il nome del software
 * il nome del maintainer
 * il nome della feature finanziabile
 * la quota necessaria per inziare lo sviluppo
 * la quota e la percentuale raggiunta dalla raccolta
 * il tempo rimasto per completare la raccolta

Le feature sono elencate per tempo rimanente al loro finanziamento.

## API

L'API contiene solamente gli oggetti finanziabili, chiamati genericamente
*feature*.

L'oggetto feature ha un riferimento all'ID del software in developers italia
dal quale è possibile recuperare tutte le informazioni del software e del 
maintainer e contiene anche i riferimenti alle _promesse_ di finanziamento
delle PA interessate a finanziare lo sviluppo del software.

### Esempio

Negli esempi si usa il cliente da linea di comando [Httpie](https://httipie.org)

La feature 1 ad esempio contiene questi dati:

    http get https://openraise.opencontent.it/feature/1

Prendendo il `software_id` da questa response è possibile interrogare le API
di developers italia con la chiamata:

    http get https://elasticsearch.developers.italia.it/jekyll/software/_search query:='{ "ids": { "values" : [ "cf016d721e1b2b6b93a665e821f6e6b1a06420b2" ] } }'
    
Per recuperare i dati dell'ente è possibile consultare l'endpoint:

    https://openraise.opencontent.it/backers
    
    